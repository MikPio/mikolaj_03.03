package com.company;

import java.util.Scanner;

public class Zadanie6 {

    public void stopaInwestycji(){
        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj wartosc inwestycji: ");
        double wartosc = scan.nextDouble();
        System.out.println("Podaj roczne oprocentowanie: ");
        double oprocentowanie = scan.nextDouble();
        System.out.println("Podaj czas inwestycji w latach: ");
        int czasInwestycji = scan.nextInt();

        int stopa = 0;
        for(int i = 0; i<czasInwestycji; i++){
            stopa += wartosc*oprocentowanie;
        }
        System.out.println("Stopa inwestycji wyniesie: " + stopa);

    }


}
