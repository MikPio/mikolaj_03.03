package com.company;

public class Zadanie3 {


    //Tablica liczb

    private int tablica[];

    public void obliczTablice(int tablica[]) {
        this.tablica = tablica;

        //a) wypisanie liczb
        System.out.print("A: ");
        for(int i=0; i<tablica.length; i++){
            System.out.print(tablica[i] + ", ");
        }


        //b) od końca
        System.out.println();
        System.out.print("B: ");
        for(int i=(tablica.length-1); i>=0; i--){
            System.out.print(tablica[i] + ", ");
        }

        //c) wszystkie na nieparzystych pozycjach
        System.out.println();
        System.out.print("C: ");
        for(int i=0; i<tablica.length; i+=2){
            System.out.print(tablica[i] + ", ");
        }

        //d) wszystkie podzielne przez 3
        System.out.println();
        System.out.print("D: ");
        for(int i=0; i<tablica.length; i++){
            if(tablica[i]%3 == 0){
                System.out.print(tablica[i] + ", ");
            }
        }

        //e) suma wszystkich
        System.out.println();
        int suma = 0;
        for(int i=0; i<tablica.length; i++){
            suma += tablica[i];
        }
        System.out.print("E: " + suma);

        //f) suma pierwszych 4
        System.out.println();
        suma = 0;
        for(int i=0; i<4; i++){
            suma += tablica[i];
        }
        System.out.print("F: " + suma);

        //g) suma ostatnich 5 większych od 2
        System.out.println();
        suma = 1;
        int limit = 0;
        for(int i=(tablica.length-1); i>=0; i--){
            if(tablica[i]>2 && limit<5){
                suma += tablica[i];
                limit ++;
            }
        }
        System.out.print("G: " + suma);

        //h) ilość liczb idąc od początku tablicy by ich suma przekroczyła 10
        System.out.println();
        suma = 1;
        int licznik = 0;
        for(int i=0; i<tablica.length; i++){
            if(suma < 10){
                suma += tablica[i];
                licznik++;
            }
        }
        System.out.print("H: " + licznik);



    }
}
